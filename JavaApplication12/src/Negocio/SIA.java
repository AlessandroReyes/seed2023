/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Estudiante;
import Util.seed.ArchivoLeerURL;
import Util.seed.ListaCD;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SIA {

    private ListaCD<Estudiante> fisica;
    private ListaCD<Estudiante> estructuras;
    private ListaCD<Estudiante> poo;

    public SIA(String urlFisica, String urlEstructuras, String urlPoo) {
        this.fisica = crear(urlFisica);
        this.estructuras = crear(urlEstructuras);
        this.poo = crear(urlPoo);
    }

    private ListaCD<Estudiante> crear(String url) {
        ListaCD<Estudiante> l = new ListaCD();
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            String datos[] = fila.split(",");
            long codigo = Long.parseLong(datos[0]);
            String nombre = datos[1];
            float p1 = Float.parseFloat(datos[2]);
            float p2 = Float.parseFloat(datos[3]);
            float p3 = Float.parseFloat(datos[4]);
            float exm = Float.parseFloat(datos[5]);
            Estudiante nuevo = new Estudiante(codigo, nombre, p1, p2, p3, exm);
            l.insertarFinal(nuevo);
        }
        return l;
    }

    public String getListadoFisica() {
        String msg = "";
        for (Estudiante dato : this.fisica) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<String> getListadoFinal() {
        ListaCD<String> resultado = new ListaCD();
        for (Estudiante x : this.fisica) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m + "\n");
        }
        return resultado;
    }

    public ListaCD<String> getListadoFinalEstructuras() {
        ListaCD<String> resultado = new ListaCD();
        for (Estudiante x : this.estructuras) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m + "\n");
        }
        return resultado;
    }

    public String getListadoPoo() {
        String msg = "";
        for (Estudiante dato : this.poo) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<String> getListadoFinalPoo() {
        ListaCD<String> resultado = new ListaCD();
        for (Estudiante x : this.poo) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m + "\n");
        }
        return resultado;
    }

    public ListaCD<Estudiante> getEstudiantesRepetidos() {
        ListaCD<Estudiante> estudiantesRepetidos = new ListaCD<>();

        for (Estudiante estudianteFisica : fisica) {
            if (estructuras.contiene(estudianteFisica) && poo.contiene(estudianteFisica)) {
                estudianteFisica.setMateriasCursando("Física, Estructuras, POO");
                estudiantesRepetidos.insertarFinal(estudianteFisica);
            }
        }

        for (Estudiante estudianteEstructuras : estructuras) {
            if (poo.contiene(estudianteEstructuras) && !estudiantesRepetidos.contiene(estudianteEstructuras)) {
                estudianteEstructuras.setMateriasCursando("Estructuras, POO");
                estudiantesRepetidos.insertarFinal(estudianteEstructuras);
            }
        }

        return estudiantesRepetidos;
    }

    public ListaCD<Estudiante> getEstudiantesCursandoDosOMas() {
        ListaCD<Estudiante> estudiantesCursandoDosOMas = new ListaCD<>();

        for (Estudiante estudianteFisica : fisica) {
            if (estructuras.contiene(estudianteFisica) || poo.contiene(estudianteFisica)) {
                if (!estudiantesCursandoDosOMas.contiene(estudianteFisica)) {
                    estudianteFisica.setMateriasCursando("Física");
                    if (poo.contiene(estudianteFisica)) {
                        estudianteFisica.setMateriasCursando(estudianteFisica.getMateriasCursando() + ", POO");
                    }
                    if (estructuras.contiene(estudianteFisica)) {
                        estudianteFisica.setMateriasCursando(estudianteFisica.getMateriasCursando() + ", Estructuras");
                    }
                    estudiantesCursandoDosOMas.insertarFinal(estudianteFisica);
                }
            }
        }

        for (Estudiante estudianteEstructuras : estructuras) {
            if (poo.contiene(estudianteEstructuras) && !estudiantesCursandoDosOMas.contiene(estudianteEstructuras)) {
                estudianteEstructuras.setMateriasCursando("Estructuras, POO");
                estudiantesCursandoDosOMas.insertarFinal(estudianteEstructuras);
            }
        }

        return estudiantesCursandoDosOMas;
    }

    public void getInformePDF() {
        try {
            ListaCD<Estudiante> estudiantesRepetidos = getEstudiantesRepetidos();
            new ImpresoraInformeSIA().imprimirListadoPromedio(getListadoEstudiantesRepetidos(estudiantesRepetidos));
        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(SIA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ListaCD<String> getListadoEstudiantesRepetidos(ListaCD<Estudiante> estudiantesRepetidos) {
        ListaCD<String> resultado = new ListaCD<>();
        for (Estudiante x : estudiantesRepetidos) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getMateriasCursando() + "-" + x.getPromedio() + "-" + m + "\n");
        }
        return resultado;
    }
}

