/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Negocio.SIA;

/**
 * Clase de prueba para el Sistema de Información Académica (SIA).
 * 
 * @author DOCENTE
 */
public class TestSIA {
    public static void main(String[] args) {
        SIA sia = new SIA("https://gitlab.com/AlessandroReyes/seed2023/-/raw/main/fisica.csv",
                "https://gitlab.com/AlessandroReyes/seed2023/-/raw/main/estructuras.csv",
                "https://gitlab.com/AlessandroReyes/seed2023/-/raw/main/poo.csv");

        System.out.println("Física:\n" + sia.getListadoFisica());
        System.out.println("Estructuras:\n" + sia.getListadoFinalEstructuras().toString());
        System.out.println("POO:\n" + sia.getListadoFinalPoo().toString());

        sia.getInformePDF();

        System.out.println("Estudiantes Repetidos:\n" + sia.getEstudiantesRepetidos().toString());

        System.out.println("Estudiantes Cursando Dos o Más Materias:\n" + sia.getEstudiantesCursandoDosOMas().toString());
    }
}

